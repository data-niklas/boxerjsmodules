const fs = require("fs")
const path = require("path")

const storageFolder = path.join(__dirname,"./storage/");
const logFile = path.join(__dirname,"./tokens.json");

var tokens = JSON.parse(fs.readFileSync(logFile));

var deleteFolderRecursive = function(path) {
    if( fs.existsSync(path) ) {
      fs.readdirSync(path).forEach(function(file){
        var curPath = path + "/" + file;
        if(fs.lstatSync(curPath).isDirectory()) { // recurse
          deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(path);
    }
};

function deleteOldInstances(token, metadata){
    for (var dir in tokens){
        if (dir != token){
            var obj = tokens[dir];
            if (obj.name == metadata.name && obj.author == metadata.author && obj.description == metadata.description){
                delete tokens[dir];
            }
        }
    }
}

function keyPath(token,key){
    return path.join(storageFolder,token,'./' + key)
}

function saveToFile(token,key,obj){
    fs.writeFileSync(keyPath(token,key),JSON.stringify(obj));
}

function loadFromFile(token,key){
    return JSON.parse(fs.readFileSync(keyPath(token,key)));
}


function storageExists(token){
    return fs.existsSync(path.join(storageFolder,token))
}

function keyExists(token,key){
    return fs.existsSync(keyPath(token,key));
}

function deleteStorage(){
    if (storageExists(this.token)){
        deleteFolderRecursive(path.join(storageFolder,this.token))
        return true;
    }
    else return false;
}

function save(key,obj){
    if (!storageExists(this.token)){
        fs.mkdirSync(path.join(storageFolder,this.token))
        deleteOldInstances(this.token,this.metadata);
        tokens[this.token] = this.metadata;
    }
    saveToFile(this.token,key,obj);
    return true;
}

function load(key){
    if (keyExists(this.token,key))return loadFromFile(this.token,key);
   
    else return undefined
}

function has(key){
    return keyExists(this.token,key);
}

function unload(){
    fs.writeFileSync(logFile,JSON.stringify(tokens));
}

function remove(key){
    if(keyExists(this.token,key)){
        fs.unlinkSync(keyPath(this.token,key));
        return true;
    }
    else return false;
}

module.exports = {save,load,has,deleteStorage,unload,remove}